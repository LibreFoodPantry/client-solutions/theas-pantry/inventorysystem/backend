# Automated Testing

We use scripts to call other scripts that run the tests.

```bash
bin/test.sh
```

This runs `bin/test.sh` which will validate the OpenAPI specification using [Swagger/OpenAPI CLI](https://www.npmjs.com/package/swagger-cli).
