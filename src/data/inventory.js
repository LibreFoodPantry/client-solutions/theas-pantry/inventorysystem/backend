/**
 * inventory.js is responsible for manipulating the items in the database. (Temporary description)
 */
//const Database = require("./database");
/* const logger = require('../lib/logger')

class Inventory {
    static async addInventoryItem(itemData) {
        const inventoryCollection = await getInventoryCollection()
        const result = await inventoryCollection.insertOne(itemData)
        let item = await inventoryCollection.findOne({ _id: result.insertedId })
        item._id = item._id.toHexString()
        return item
    }

    static async getInventory() {
        let totalWeight
        const inventoryCollection = await getInventoryCollection()
        const inventory_cursor = inventoryCollection.find()
        let weight = await inventory_cursor.toArray()
        weight.forEach((item) => {
            totalWeight += item._id
        })

        return totalWeight
    }

    static async removeInventoryItem(itemData) {
        const inventoryCollection = await getItemsCollection()
        const result = await inventoryCollection.deleteOne({
            _id: ObjectID(itemData),
        })
        return result.deletedCount >= 1
    }

    static async getInventoryCollection() {
        const database = await Database.get()
        return database.db('inventory').collection('inventory')
    }
}
module.exports = Inventory
 */

/**
 * inventory.js is responsible for manipulating the items in the database. (Temporary description)
 */


'use strict'
const Database = require("../lib/database");
const logger = require('../lib/logger')
const { ObjectId } = require('mongodb');


class Inventory {
  static async updateInventoryWeight(itemWeight) {
    const inventoryCollection = await Inventory.getInventoryCollection();

    await inventoryCollection.updateOne(
      {},
      {$inc: {weight: itemWeight}}
    );

    const newWeight = await Inventory.getInventory();

    return newWeight;
  }


  static async getInventory() {
    const inventoryCollection = await Inventory.getInventoryCollection();
    const inventoryWeight = await inventoryCollection.findOne();

    return inventoryWeight ? inventoryWeight.weight : 0;
  }

  /*
  static async removeInventoryItem(weight) {
    const inventoryCollection = await Inventory.getInventoryCollection();
    const result = await inventoryCollection.deleteOne({ _id: ObjectId(weight) });
    return result.deletedCount >= 1;
  }
  **/
  
    static async getInventoryCollection() {
        const database = await Database.get();
        const inventoryCollection = database.db('inventory').collection('inventory');

        if(await inventoryCollection.findOne() == null){
          await inventoryCollection.insertOne({weight: 0});
        }

        return inventoryCollection
    }
}

module.exports = Inventory
