/**
 * config.js supplies configuration to the rest of the system.
 * It pulls some of its values from environment variables, and sets defaults
 * values when they are not provided.
 *
 * Example Usage:
 *    const config = require("./config.js");
 *    const rootDirectory = config.ROOT_DIR;
 */

// Load path, a builtin library for manipulating file paths.
"use strict";
const path = require('path');
const { readFileSync } = require('fs');
const yaml = require('js-yaml');
const ROOT_DIR = path.join(__dirname, "..", "..");

const config = {
  MONGO_URI: process.env.MONGO_URI,
  SERVER_PORT: process.env.SERVER_PORT || 10201,
  OPENAPI_FILE: path.join(ROOT_DIR, 'specification', 'openapi.yaml'),
  ENDPOINTS_DIR: path.join(ROOT_DIR, "src", "endpoints"),

};

const apiSchema = yaml.load(readFileSync(config.OPENAPI_FILE, 'utf8'));
config.API_VERSION = apiSchema.info.version;

module.exports = config;
