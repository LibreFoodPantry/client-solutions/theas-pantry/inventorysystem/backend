/**
 * 2/10/22
 * This was moved over from the send.js folder, I think that as of right now we need to figure out how all this works before we can actually write a full test
 * 
 *  2/15/22 
 *  Upon further clarification, this is a crude test for Rabbit MQ just to see how it works, might be helpful just to help figure out Rabbit MQ when we get there
 * 
 * Using https://gitlab.com/LibreFoodPantry/training/microservices/microservices-examples/backend-for-frontend-code/-/tree/main/testing/test for guidance on this
*/

let data = {
    name: 'Austin Engel',
    company: 'Spotify',
    designation: 'Senior Software Engineer'
};
let data1 = {
    name: 'Derin Sabu',
    company: 'Apple',
    designation: 'Senior Software Engineer'
};
let data2 = {
    name: 'Faraaz Baig',
    company: 'Google',
    designation: 'Data Scientist'
};
let data3 = {
    name: 'Haoru Song',
    company: 'Amazon',
    designation: 'Senior Front End Developer'
};

const amqp = require('amqplib/callback_api');

//create connection
amqp.connect('amqp://localhost', function(connError, connection){
    if(connError) throw connError;
    //create channel
    connection.createChannel(function(channError, channel){
        if(channError) throw channError;
        //assert queue
        const queue = 'inventory'
        channel.assertQueue(queue);

        //send message to queue
        channel.sendToQueue(queue, Buffer.from(JSON.stringify(data)));
        console.log(`message sent to ${queue}`);

        channel.sendToQueue(queue, Buffer.from(JSON.stringify(data1)));
        console.log(`message sent to ${queue}`);

        channel.sendToQueue(queue, Buffer.from(JSON.stringify(data2)));
        console.log(`message sent to ${queue}`);

        channel.sendToQueue(queue, Buffer.from(JSON.stringify(data3)));
        console.log(`message sent to ${queue}`);
    });
    connection.createChannel(function(channError, channel){
        if(channError) throw channError;
        //assert queue
        const queue = 'guestinfo'
        channel.assertQueue(queue);

        //send message to queue
        channel.sendToQueue(queue, Buffer.from(JSON.stringify(data)));
        console.log(`message sent to ${queue}`);

        channel.sendToQueue(queue, Buffer.from(JSON.stringify(data1)));
        console.log(`message sent to ${queue}`);

        channel.sendToQueue(queue, Buffer.from(JSON.stringify(data2)));
        console.log(`message sent to ${queue}`);

        channel.sendToQueue(queue, Buffer.from(JSON.stringify(data3)));
        console.log(`message sent to ${queue}`);
    });
});
