"use strict";
const Inventory = require("../data/inventory.js");

module.exports = {
  method: 'patch',
  path: '/inventory',
  async handler(request, response) {

    // Log incoming request
    console.log("Received PATCH request to /inventory with weight", request.body.weight);
    const itemWeight = request.body.weight;

    if (!itemWeight) {
      console.error("Missing weight in request body.");
      return response.status(400).json({ error: "Weight is required "});
    }
    
    
    try {
      // Update inventory with provided weight
      const inventory = await Inventory.updateInventoryWeight(itemWeight);
      console.log("Updated inventory", inventory);
      

      // Assuming updatedInventory contains the new total weight
      response.status(200).json(inventory);
    } catch (e) {
      // Log any errors
      console.error("Error updating inventory:", e);
      return response.status(500).json({
        error: 'Internal Server Error',
        details: e.message || e,
      });
    }
  }
};
